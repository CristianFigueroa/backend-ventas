package cl.ufro.dci.crudventas.configuration;

import cl.ufro.dci.crudventas.entity.Rol;
import cl.ufro.dci.crudventas.enums.RolNombre;
import cl.ufro.dci.crudventas.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.NoSuchElementException;

@Configuration
public class BBDD {

    @Autowired
    RolService rolService;

    @Bean
    public void generateBBDD(){
        try{
            rolService.getByRolNombre(RolNombre.ROLE_USER).get();
        }catch(NoSuchElementException e){
            rolService.save(new Rol(RolNombre.ROLE_USER));
            rolService.save(new Rol(RolNombre.ROLE_ADMIN));

        }
    }

}
