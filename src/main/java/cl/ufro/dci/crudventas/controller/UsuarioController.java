package cl.ufro.dci.crudventas.controller;

import cl.ufro.dci.crudventas.dto.Mensaje;
import cl.ufro.dci.crudventas.dto.UsuarioDto;
import cl.ufro.dci.crudventas.entity.Usuario;
import cl.ufro.dci.crudventas.service.RolService;
import cl.ufro.dci.crudventas.service.UsuarioService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuario")
//@CrossOrigin("http://localhost:4200") //recurrir al servicio desde cualquier url
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    RolService rolService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/lista")
    public ResponseEntity<List<Usuario>> list() {
        List<Usuario> list = usuarioService.list();
        return new ResponseEntity<List<Usuario>>(list, HttpStatus.OK);
    }

    @GetMapping("/detalle/{id}")
    public ResponseEntity<Usuario> getById(@PathVariable("id") int id) {
        if (!usuarioService.existsById(id))
            return new ResponseEntity(new Mensaje("El usuario no existe"), HttpStatus.BAD_REQUEST);
        Usuario usuario = usuarioService.getById(id).get();
        return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
    }


    @PutMapping("/editar/{id}")
    public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody UsuarioDto usuarioDto) {
        if (!usuarioService.existsById(id))
            return new ResponseEntity(new Mensaje("El usuario no existe"), HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(usuarioDto.getNombre()))
            return new ResponseEntity(new Mensaje("El nombre no puede estar en blanco"), HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(usuarioDto.getPassword()))
            return new ResponseEntity(new Mensaje("El password no puede estar en blanco"), HttpStatus.BAD_REQUEST);
        if (usuarioService.existsByNombreUsuario(usuarioDto.getNombre()) && usuarioService.getByNombreUsuario(usuarioDto.getNombre()).get().getId() != id)
            return new ResponseEntity(new Mensaje("El nombre de usuario ya esta en uso"), HttpStatus.BAD_REQUEST);

        Usuario usuario = usuarioService.getById(id).get();
        usuario.setNombre(usuarioDto.getNombre());
        usuario.setNombreUsuario(usuarioDto.getUsername());
        usuario.setEmail(usuarioDto.getEmail());
        usuario.setPassword(usuarioDto.getPassword());

        usuarioService.save(usuario);
        return new ResponseEntity(new Mensaje("Producto editado correctamente"), HttpStatus.OK);
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") int id) {
        if (!usuarioService.existsById(id))
            return new ResponseEntity(new Mensaje("El usuario no existe"), HttpStatus.BAD_REQUEST);
        usuarioService.delete(id);
        return new ResponseEntity(new Mensaje("Usuario eliminado correctamente"), HttpStatus.OK);
    }


}
