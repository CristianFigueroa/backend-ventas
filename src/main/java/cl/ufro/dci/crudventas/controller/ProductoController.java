package cl.ufro.dci.crudventas.controller;

import cl.ufro.dci.crudventas.dto.Mensaje;
import cl.ufro.dci.crudventas.dto.ProductoDto;
import cl.ufro.dci.crudventas.entity.Producto;
import cl.ufro.dci.crudventas.service.ProductoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/producto")
//@CrossOrigin("http://localhost:4200") //recurrir al servicio desde cualquier url
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @PreAuthorize("permitAll()")
    @GetMapping("/lista")
    public ResponseEntity<List<Producto>> list(){
        List<Producto> list = productoService.list();
        return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
    }

    @GetMapping("/detalle/{id}")
    public ResponseEntity<Producto> getById(@PathVariable("id") int id){
        if( ! productoService.existsById(id))
            return new ResponseEntity(new Mensaje("El producto no existe"),HttpStatus.BAD_REQUEST);
        Producto producto = productoService.getById(id).get();
        return new ResponseEntity<Producto>(producto, HttpStatus.OK);
    }

    @GetMapping("/detallepornombre/{nombre}")
    public ResponseEntity<Producto> getByNombre(@PathVariable("nombre") String nombre){
        if( ! productoService.existsByNombre(nombre))
            return new ResponseEntity(new Mensaje("El producto no existe"),HttpStatus.BAD_REQUEST);
        Producto producto = productoService.getByNombre(nombre).get();
        return new ResponseEntity<Producto>(producto, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/crear")
    public ResponseEntity<?> create(@RequestBody ProductoDto productoDto){
        if(StringUtils.isBlank(productoDto.getNombre()))
            return new ResponseEntity(new Mensaje("El nombre no puede estar en blanco"),HttpStatus.BAD_REQUEST);
        if(productoDto.getPrecio() == null || productoDto.getPrecio() < 0)
            return new ResponseEntity(new Mensaje("El precio debe ser mayor que 0"),HttpStatus.BAD_REQUEST);
        if(productoService.existsByNombre(productoDto.getNombre()))
            return new ResponseEntity(new Mensaje("El nombre ya existe"),HttpStatus.BAD_REQUEST);

        Producto producto = new Producto(productoDto.getNombre(),productoDto.getPrecio());
        productoService.save(producto);
        return new ResponseEntity(new Mensaje("Producto creado correctamente"),HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/editar/{id}")
    public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody ProductoDto productoDto){
        if( ! productoService.existsById(id))
            return new ResponseEntity(new Mensaje("El producto no existe"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(productoDto.getNombre()))
            return new ResponseEntity(new Mensaje("El nombre no puede estar en blanco"),HttpStatus.BAD_REQUEST);
        if(productoDto.getPrecio() < 0)
            return new ResponseEntity(new Mensaje("El precio debe ser mayor que 0"),HttpStatus.BAD_REQUEST);
        if(productoService.existsByNombre(productoDto.getNombre()) && productoService.getByNombre(productoDto.getNombre()).get().getId() != id)
            return new ResponseEntity(new Mensaje("El nombre ya esta en uso"),HttpStatus.BAD_REQUEST);

        Producto producto = productoService.getById(id).get();
        producto.setNombre(productoDto.getNombre());
        producto.setPrecio(productoDto.getPrecio());
        productoService.save(producto);
        return new ResponseEntity(new Mensaje("Producto editado correctamente"),HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") int id){
        if( ! productoService.existsById(id))
            return new ResponseEntity(new Mensaje("El producto no existe"), HttpStatus.BAD_REQUEST);
        productoService.delete(id);
        return new ResponseEntity(new Mensaje("Producto eliminado correctamente"), HttpStatus.OK);
    }



}
