package cl.ufro.dci.crudventas.service;

import cl.ufro.dci.crudventas.dto.UsuarioDto;
import cl.ufro.dci.crudventas.entity.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class UsuarioDetailServiceImpl implements UserDetailsService {

    @Autowired
    UsuarioService usuarioService;

    // AUTENTIFICADOR
    @Override
    public UserDetails loadUserByUsername(String nombreUsuario) throws UsernameNotFoundException {
        Usuario usuario = usuarioService.getByNombreUsuario(nombreUsuario).get();
        return UsuarioDto.build(usuario);
    }

}
