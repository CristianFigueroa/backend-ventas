package cl.ufro.dci.crudventas.service;

import cl.ufro.dci.crudventas.entity.Producto;
import cl.ufro.dci.crudventas.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    public List<Producto> list() {
        return productoRepository.findAll();
    }

    //Optional : si no existe no arroja error
    public Optional<Producto> getById(int id) {
        return productoRepository.findById(id);
    }

    //Optional : si no existe no arroja error
    public Optional<Producto> getByNombre(String nombre) {
        return productoRepository.findByNombre(nombre);
    }

    public void save(Producto producto) {
        productoRepository.save(producto);
    }

    public void delete(int id) {
        productoRepository.deleteById(id);
    }

    public boolean existsById(int id) {
        return productoRepository.existsById(id);
    }

    public boolean existsByNombre(String nombre) {
        return productoRepository.existsByNombre(nombre);
    }
}
