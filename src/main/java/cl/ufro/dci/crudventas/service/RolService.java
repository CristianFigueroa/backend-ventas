package cl.ufro.dci.crudventas.service;

import cl.ufro.dci.crudventas.entity.Rol;
import cl.ufro.dci.crudventas.enums.RolNombre;
import cl.ufro.dci.crudventas.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service("rolService")
@Transactional
public class RolService {

    @Autowired
    RolRepository rolRepository;

    public Optional<Rol> getByRolNombre(RolNombre rolNombre) {
        return rolRepository.findByRolNombre(rolNombre);
    }

    public void delete(int id) {
        rolRepository.deleteById(id);
    }

    public void save(Rol rol) {
        rolRepository.save(rol);
    }
}
