package cl.ufro.dci.crudventas.enums;

public enum RolNombre {

    ROLE_ADMIN,
    ROLE_USER
}
