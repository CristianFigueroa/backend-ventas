package cl.ufro.dci.crudventas.repository;

import cl.ufro.dci.crudventas.entity.Rol;
import cl.ufro.dci.crudventas.enums.RolNombre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RolRepository extends JpaRepository<Rol, Integer> {
    Optional<Rol> findByRolNombre(RolNombre rolNombre);
}
