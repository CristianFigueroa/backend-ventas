# Backend Ventas

ApiRest construido con SpringBoot para el backend, Mysql para la BBDD para simular manejo de productos para una tienda y autentificación basada en tokens

_Este repositorio abarca solo el backend de la aplicación, para ver el frontend ingresa al siguiente link:_
[Frontend](https://gitlab.com/CristianFigueroa/frontend_ventas)

### Despliegue maven

Antes necesitas crear una database en el puerto 3306 con el nombre "salaventas", User: user, pass: pass. Si deseas cambiar estos valores debes editar el archivo _application-dev.properties_
```
mysql> CREATE DATABASE salaventas;
```

Luego ejecuta normalmente: 
```
mvn spring-boot:run
```

### Despliegue por docker

_El proyecto cuenta con distintos perfiles creados, a continuación se explicara el despliegue del ambiente de producción_

Clona el repositorio
```
git clone https://gitlab.com/CristianFigueroa/backend-ventas.git
```
Crea un archivo .env un directorio más arriba que el proyecto, con los siguientes datos (serán los datos de la BBDD, puedes editarlo a tu gusto!)
_Esta es la data sensible de la aplicación, por ello no se guarda en el repositorio_

```
MYSQL_USER=user
MYSQL_PASSWORD=pass
MYSQL_ROOT_PASSWORD=pass
MYSQL_DATABASE=database

# Esto se inyecta luego en application.properties
SPRING_DATASOURCE_USERNAME=user
SPRING_DATASOURCE_PASSWORD=pass
SPRING_DATASOURCE_URL=jdbc:mysql://mysql-service/database?serverTimezone=UTC&useSSL=true&useUnicode=yes&characterEncoding=UTF-8
```

Para ejecutar el contenedor, ejecuta en la terminal:
```
docker-compose up --build
```


## Construido con 

* [Spring-boot](https://spring.io) - Framework del backend
* [JWT](https://jwt.io) - Estándar de creación de tokens para la autentificación
* [MySQL](https://https://www.mysql.com) - Base de datos
* [Maven](https://maven.apache.org) - Control de dependencias
* [Angular](https://angular.io) - Framework del frontend



## Autores 

* **Cristian Figueroa** - *Trabajo Inicial* - [cristianfigueroa](https://gitlab.com/CristianFigueroa)
