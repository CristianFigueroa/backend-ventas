# stage build
# For Java 8, try this
# FROM openjdk:8-jdk-alpine

# For Java 11, try this
FROM adoptopenjdk/openjdk11:alpine as build

# Especifica el directorio en la imagen y se posiciona en el
WORKDIR /app

# Copia los archivos en /app
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .

# Permisos de ejecucion
RUN chmod +x ./mvnw

# Descarga las dependencias si es necesario
RUN ./mvnw dependency:go-offline -B

COPY src src

# creacion del .jar
RUN ./mvnw package -DskipTests

# crea el directorio target/dependency
RUN mkdir -p target/dependency
# Crea un jar separado para cada dependencia y crea un pequeño jar para la app
RUN cd target/dependency; jar -xf ../*.jar

# Production Stage for Spring boot application image
FROM adoptopenjdk/openjdk11:alpine-jre as production

ARG DEPENDENCY=/app/target/dependency

# Copia del stage build las dependencias
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app

# java -cp :se indica el directorio en el que esta el proyecto
# app :el directorio del proyecto
# -Dspring.profiles.active=dev se usa application-prod.properties
ENTRYPOINT ["java", "-cp", "app:app/lib/*","-Dspring.profiles.active=prod","cl.ufro.dci.crudventas.CrudVentasApplication"]
